<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
 <link rel="stylesheet" href="/SolrApp/css/layui.css?t=1542630986934"  media="all">
 <link rel="stylesheet" href="/SolrApp/css/globle.css?t=1542630986934"  media="all">
</head>
<body>
<div class="site-text site-block" style="text-align: center;width: 530px;margin-left: 550px;">
   <div class="site-title">
      <fieldset><legend><a name="pane">添加商品</a></legend></fieldset>
    </div>
    <form class="layui-form layui-form-pane" id="form1" action="/SolrApp/addProduct" method="post">
	  <div class="layui-form-item">
	    <label class="layui-form-label">商品标题</label>
	    <div class="layui-input-inline">
	      <input type="text"name="title" lay-verify="required" placeholder="请输入标题" autocomplete="off" class="layui-input">
	    </div>
	  </div>
	  <div class="layui-form-item">
	    <label class="layui-form-label">商品描述</label>
	    <div class="layui-input-inline">
	      <input type="text" name="content" lay-verify="required" placeholder="请输入描述" autocomplete="off" class="layui-input">
	    </div>
	  </div>
	  <div class="layui-form-item">
	    <label class="layui-form-label">商品价格</label>
	    <div class="layui-input-inline">
	      <input type="text" name="price" lay-verify="required" placeholder="请输入价格" autocomplete="off" class="layui-input">
	    </div>
	  </div>
	  <div class="layui-form-item">
	    <div class="layui-input-block">
	      <button class="layui-btn" lay-submit="" lay-filter="demo1">立即提交</button>
	      <button type="button" class="layui-btn layui-btn-primary" onclick="window.location='/SolrApp/listProduct'">跳转列表页</button>
	    </div>
	  </div>
	</form>
	</div>
	<script src="/SolrApp/layui/layui.js" charset="utf-8"></script>
		<script>
		layui.use(['form', 'layedit', 'laydate'], function(){
		  var form = layui.form
		  ,layer = layui.layer
		  ,layedit = layui.layedit
		  ,laydate = layui.laydate;
		  
		  //日期
		  laydate.render({
		    elem: '#date'
		  });
		  laydate.render({
		    elem: '#date1'
		  });
		  
		  //自定义验证规则
		  form.verify({
		    title: function(value){
		      if(value.length < 0){
		        return '标题不能为空';
		      }
		    },
			content: function(value){
		      if(value.length < 0){
			        return '描述不能为空';
			      }
			 },
			    price: function(value){
				      if(value.length < 0){
					        return '价格不能为空';
					  }else{
						  
					  }
					}
		  });
		  
		  //监听提交
		  form.on('submit(demo1)', function(data){
			  var index = layer.load(1, {
				  shade: [0.1,'#000'] //0.1透明度的白色背景
				});
			  var form = document.getElementById('form1');
			   //再次修改input内容
			   form.submit();
		  });
		});
		</script>
</body>
</html>