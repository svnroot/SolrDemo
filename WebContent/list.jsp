<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
 <link rel="stylesheet" href="/SolrApp/css/layui.css?t=1542630986934"  media="all">
 <link rel="stylesheet" href="/SolrApp/css/globle.css?t=1542630986934"  media="all">
</head>
<script src="/SolrApp/layui/layui.js" charset="utf-8"></script>
<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
<script>
layui.use('table', function(){
  var table = layui.table;
  table.render({
    elem: '#test'
    ,url:'/SolrApp/listProduct'
    ,toolbar: '#toolbarDemo'
    ,defaultToolbar:[]
    ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
    ,cols: [[
      {type:'checkbox'},
      {field:'id', width:300, title: '产品ID',align: 'center'}
      ,{field:'title', width:400, title: '产品标题',align: 'center'}
      ,{field:'price', width:110, title: '产品价格', sort: true,align: 'center'}
      ,{field:'content', title: '产品描述',align: 'center'}
    ]] ,page: true ,id: 'testReload'
  });
  
  var $ = layui.$, active = {
		    reload: function(){
		      var demoReload = $('#demoReload');
		      //执行重载
		      table.reload('testReload', {
		        page: {
		          curr: 1 //重新从第 1 页开始
		        },
		        method:"post"
		        ,where: {
		        	keyWord:demoReload.val()
		        }
		      });
		    }
		  };
		  
		  $('.demoTable .layui-btn').on('click', function(){
		    var type = $(this).data('type');
		    active[type] ? active[type].call(this) : '';
		  });
		  
		  //头工具栏事件
		  table.on('toolbar(test)', function(obj){
		    var checkStatus = table.checkStatus(obj.config.id);
		    switch(obj.event){
		      case 'add':
		        window.location='/SolrApp/add.jsp';
		      break;
		      case 'getCheckLength':
		        var data = checkStatus.data;
		        layer.msg('选中了：'+ data.length + ' 个');
		      break;
		      case 'del':
		    	  var data = checkStatus.data;
		    	  console.log(data);
		    	  if(data.length==0){
		    		  layer.msg('请选择要删除的数据');
		    	  }else{
		    		  layer.confirm('确定要删除这'+data.length+'条数据?', {icon: 2, title:'提示'}, function(index){
		    			  
			    		  layer.close(index);
			    		});
		    	  }
		    	 
		      break;
		    };
		  });
});
</script>
<body>
 <div class="site-title">
  <fieldset><legend><a name="pane">商品列表</a></legend></fieldset>
 </div>
 <div class="demoTable">
  关键字：
  <div class="layui-inline">
  <input class="layui-input" name="keyWord" id="demoReload" autocomplete="off">
  </div>
	<button class="layui-btn" data-type="reload">搜索</button>
  </div>
  <table class="layui-hide" id="test" lay-filter="test"></table>
  <script type="text/html" id="toolbarDemo">
  <div class="layui-btn-container">
   <button class="layui-btn" lay-event="add">
     <i class="layui-icon">&#xe608;</i> 添加
   </button>
    <button class="layui-btn layui-btn-danger" lay-event="del">
      <i class="layui-icon">&#xe640;</i> 删除
    </button>
  </div>
</script>
</body>
</html>