package com.casic.service;

import java.util.List;

import com.casic.bean.Product;
import com.casic.dao.ProductDao;

public class ProductService {
	
	public String add(Product product) {
		
		ProductDao productDao=new ProductDao();
		
		return productDao.add(product);
	}
	
	public List<Product> list(String keyWord,int page,int limit){
		ProductDao productDao=new ProductDao();
		return productDao.list(keyWord,page,limit);
	}

}