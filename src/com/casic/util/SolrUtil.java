package com.casic.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;

import com.casic.bean.Product;

public class SolrUtil {
	
	private static HttpSolrClient client=null;
	
	
	public static void addSolr(Product product) {
		client=new HttpSolrClient.Builder().withBaseSolrUrl("http://172.17.41.84:8080/solr/solr").build();
		System.out.println("执行添加数据....");
		 SolrInputDocument document=new SolrInputDocument();
         document.addField("id",product.getId());
         document.addField("title_ik",product.getTitle());
         document.addField("content_ik",product.getContent());
         document.addField("price",product.getPrice());
         try {
			client.add(document);
			client.commit();
			client.close();
			System.out.println("执行添加成功....");
		} catch (SolrServerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("执行添加失败....");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("执行添加失败....");
		}
	}
	
	public static List<Product> listSolr(String keyWord,int page,int limit){
		client=new HttpSolrClient.Builder().withBaseSolrUrl("http://172.17.41.84:8080/solr/solr").build();
		 SolrQuery solrQuery=new SolrQuery();
		 
		 if("".equals(keyWord)||keyWord==null)
			 keyWord="*:*";
		 else
			 keyWord="title_ik:"+keyWord+" OR content_ik:"+keyWord;
         //设置关键字
         solrQuery.setQuery(keyWord);
         //设置默认域
         //solrQuery.set("df","title_ik OR content_ik");
         //设置查询字段
         //solrQuery.set("fl","title_ik,content_ik");
         //分页查询
         solrQuery.setStart(page);
         solrQuery.setRows(limit);
         //设置过滤条件
         //solrQuery.set("fq","id:[2000 TO 2055]");
		//打开高亮显示
		solrQuery.setHighlight(true);
		//设置高亮字段
		solrQuery.addHighlightField("title_ik,content_ik");
		//设置高亮前后缀样式
		solrQuery.setHighlightSimplePre("<font color='red'>");
		solrQuery.setHighlightSimplePost("</font>");
		

		List<Product> rs=new ArrayList<Product>();
		QueryResponse response=null;
		try {
			
			System.out.println("执行查询数据...."+keyWord);
			response = client.query(solrQuery);
			System.out.println("执行查询结束....");
			SolrDocumentList responseResults = response.getResults();
			
			System.out.println(responseResults.size());
			
			Map<String, Map<String, List<String>>> highlighting = response.getHighlighting();
			
			System.out.println(highlighting);
			
			for (SolrDocument document : responseResults) {
			        Product prodect=new Product();
					String id =(String) document.get("id");
					String title_ik =(String) document.get("title_ik");
					String content = (String) document.get("content_ik");
					Float price=(Float) document.get("price");
					
					Map<String, List<String>> stringListMap = highlighting.get(id);
					
					prodect.setId(id);
					if(stringListMap!=null) {
						prodect.setTitle(stringListMap.get("title_ik")!=null?stringListMap.get("title_ik").get(0):title_ik);
						prodect.setContent(stringListMap.get("content_ik")!=null?stringListMap.get("content_ik").get(0):content);
					}else{
						prodect.setTitle(title_ik);
						prodect.setContent(content);
					}
					prodect.setPrice(price);
					rs.add(prodect);
			}
			client.close();
			
		} catch (SolrServerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("执行查询失败....");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("执行查询失败....");
		}
		
		return rs;
	}
	
	public static long getTotal() {
		
		client=new HttpSolrClient.Builder().withBaseSolrUrl("http://172.17.41.84:8080/solr/solr").build();
	    SolrQuery solrQuery=new SolrQuery();
	              solrQuery.setQuery("*:*");
	     QueryResponse response;
	     long total=0;
		try {
			response = client.query(solrQuery);
			
		   SolrDocumentList responseResults = response.getResults();
		   total = responseResults.getNumFound();
		} catch (SolrServerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
		return total;
	}

}
