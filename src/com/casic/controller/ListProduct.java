package com.casic.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.casic.bean.Product;
import com.casic.service.ProductService;
import com.casic.util.SolrUtil;

import net.sf.json.JSONObject;

/**
 * Servlet implementation class ListProduct
 */
@WebServlet(name = "listProduct", urlPatterns = { "/listProduct" })
public class ListProduct extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListProduct() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
        request.setCharacterEncoding("UTF-8");
		
		String page="".equals(request.getParameter("page"))?"1":request.getParameter("page");
		String limit="".equals(request.getParameter("limit"))?"0":request.getParameter("limit");
		String keyWord=request.getParameter("keyWord");
		
		
		System.out.println("---page---->"+page);
		System.out.println("---limit---->"+limit);
		System.out.println("---keyWord---->"+keyWord);
		
		ProductService prodectService=new ProductService();
		List<Product> list=prodectService.list(keyWord,(Integer.parseInt(page)-1)*Integer.parseInt(limit),Integer.parseInt(limit));
		
		// 设置响应内容类型
		// 设置响应内容类型
		response.setContentType("text/json; charset=utf-8");
		PrintWriter out = response.getWriter();
		
		Map<String,Object> rs=new HashMap<String,Object>();
		                   rs.put("code", 0);
		                   rs.put("msg", "");
		                   rs.put("count",SolrUtil.getTotal());
		                   rs.put("data", list);
		
		out.println(JSONObject.fromObject(rs).toString());
		
	}

}
