package com.casic.controller;

import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.casic.bean.Product;
import com.casic.service.ProductService;

/**
 * Servlet implementation class AddProduct
 */
@WebServlet(name = "addProduct", urlPatterns = { "/addProduct" })
public class AddProduct extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddProduct() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
        request.setCharacterEncoding("UTF-8");
		String title=request.getParameter("title");
		String content=request.getParameter("content");
		String price=request.getParameter("price");
		
		ProductService prodectService=new ProductService();
		
		Product product=new Product();
		        product.setTitle(title);
		        product.setContent(content);
		        product.setPrice(Double.parseDouble(price));
		        
		        prodectService.add(product);
		
		request.getRequestDispatcher("/add.jsp").forward(request, response);
	}

}
