package com.casic.dao;

import java.util.List;
import java.util.UUID;

import com.casic.bean.Product;
import com.casic.util.SolrUtil;

public class ProductDao {

	public String add(Product product) {
		// TODO Auto-generated method stub
		String id=UUID.randomUUID().toString();
		       product.setId(id);
		       SolrUtil.addSolr(product);
		return id;
	}
	
	public List<Product> list(String keyWord,int page,int limit){
		return SolrUtil.listSolr(keyWord,page,limit);
	}

}
